package edu.upc.damo.botons_i_orientacio;

import android.app.Activity;
import android.os.Bundle;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.RadioGroup;


public class MainActivity extends Activity {

    RadioGroup rgHV, rgDE;
    LinearLayout pare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();

    }

    private void inicialitza() {
        rgHV =(RadioGroup) findViewById(R.id.radioGruphv);
        rgDE =(RadioGroup) findViewById(R.id.radioGrupEsqDreta);
        pare = (LinearLayout) findViewById(R.id.layoutPare);

        rgHV.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                canvi(group, checkedId);
            }
        });

        rgDE.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                canvi(group, checkedId);
            }
        });


    }

    private void canvi(RadioGroup grup, int checkedID) {
       switch (checkedID){
            case R.id.bHoritzontal:   pare.setOrientation(LinearLayout.HORIZONTAL);break;
            case R.id.bVertical:      pare.setOrientation(LinearLayout.VERTICAL);; break;
            case R.id.bCentrat:       grup.setGravity(Gravity.CENTER_HORIZONTAL); break;
            case R.id.bDreta:         grup.setGravity(Gravity.RIGHT); break;
            case R.id.bEsq:           grup.setGravity(Gravity.LEFT); break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
